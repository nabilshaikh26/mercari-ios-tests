# mercari-ios-tests

This XCUITest based sample tests project is implemented with the help of Swift & Cucumberish and uses page-object model as the design pattern with BDD approach.

**Structure:**

```

├── Mercari
├── MercariTests
├── MercariUITests
    └── Features (feature files)
    └── Step Definitions (tests code)
    └── UI Identifiers (page objects)
    └── Support
        └── Cucumberish Initializer (Initialize all step definitions)
        └── Cucumberish Loader
        └── Common Methods
        └── Common Step Definitions (It includes all reusable steps)


```
