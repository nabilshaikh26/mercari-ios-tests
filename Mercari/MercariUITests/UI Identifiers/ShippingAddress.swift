//
//  ShippingAddress.swift
//  MercariUITests
//
//  Created by Nabil Shaikh on 17/04/21.
//

import Foundation
import XCTest

class ShippingAddress {
    
    let application = XCUIApplication()
    
    func getShippingAddressTab() -> XCUIElement {
        return application.collectionViews.staticTexts["shippingAddress"]
    }
    
    func getAddressListTitle() -> XCUIElement {
        return application.navigationBars.staticTexts["addressList"]
    }
    
    func getEditButton() -> XCUIElement {
        return application.navigationBars.staticTexts["Edit"]
    }
    
    func getDeleteButton() -> XCUIElement {
        return application.navigationBars.staticTexts["Delete"]
    }
    
    func getRegisterAddressButton() -> XCUIElement {
        return application.buttons["registerAddress"]
    }
    
    func getFullNameTextField() -> XCUIElement {
        return application.staticTexts["fullName"]
    }
    
    func getAddressTextField() -> XCUIElement {
        return application.staticTexts["address"]
    }
    
    func getZipcodeTextField() -> XCUIElement {
        return application.staticTexts["zipcode"]
    }
    
    func getSaveAddressButton() -> XCUIElement {
        return application.buttons["Save"]
    }
    
    func getSaveMessageTitle(title: String) -> XCUIElement {
        return application.alerts.element.staticTexts[title]
    }
    
    func getDeleteAddressButton() -> XCUIElement {
        return application.buttons["Delete"]
    }
}
