//
//  Home.swift
//  MercariUITests
//
//  Created by Nabil Shaikh on 17/04/21.
//

import Foundation
import XCTest

class Home {
    
    let application = XCUIApplication()
    
    func getHomeTabBar() -> XCUIElement {
        return application.staticTexts["Home"]
    }
    
    func getSearchTextField() -> XCUIElement {
        return application.staticTexts["Search"]
    }
    
    func getSearchButton() -> XCUIElement {
        return application.buttons["Search"]
    }
}
