//
//  Login.swift
//  MercariUITests
//
//  Created by Nabil Shaikh on 17/04/21.
//

import Foundation
import XCTest

class Login {
    
    let application = XCUIApplication()
    
    func getUsernameTextField() -> XCUIElement {
        return application.scrollViews.otherElements.textFields["userId"]
    }
    
    func getPasswordTextField() -> XCUIElement {
        return application.scrollViews.otherElements.textFields["password"]
    }
    
    func getLoginButton() -> XCUIElement {
        return application.buttons["Log In"]
    }
    
}
