//
//  MyPage.swift
//  MercariUITests
//
//  Created by Nabil Shaikh on 17/04/21.
//

import Foundation
import XCTest

class MyPage {
    
    let application = XCUIApplication()
    
    func getMyPageTabBar() -> XCUIElement {
        return application.staticTexts["myPage"]
    }
    
    func getPersonalInformationTab() -> XCUIElement {
        return application.collectionViews.staticTexts["personalInformation"]
    }
}
