//
//  Search.swift
//  MercariUITests
//
//  Created by Nabil Shaikh on 18/04/21.
//

import Foundation
import XCTest

class Search {
    
    let application = XCUIApplication()
    
    func getMyPageTabBar() -> XCUIElement {
        return application.staticTexts["myPage"]
    }
    
    func getItemTitle() -> XCUIElement {
        return application.staticTexts["itemTitle"]
    }
}
