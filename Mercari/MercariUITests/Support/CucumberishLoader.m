//
//  CucumberishLoader.m
//  MercariUITests
//
//  Created by Nabil Shaikh on 14/04/21.
//

#import <Foundation/Foundation.h>
#import "MercariUITests-Swift.h"

void CucumberishInit(void);

__attribute__((constructor))
void CucumberishInit(){
    
    [CucumberishInitializer setupCucumberish];
}
