//
//  CommonMethods.swift
//  MercariUITests
//
//  Created by Nabil Shaikh on 18/04/21.
//

import Foundation

class CommonMethods {
    
    func parseToInt(string: String) -> Int? {
        return Int(string.components(separatedBy: CharacterSet.decimalDigits.inverted).joined())
    }
}
