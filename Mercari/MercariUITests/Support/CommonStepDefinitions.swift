//
//  CommonStepDefinitions.swift
//  MercariUITests
//
//  Created by Nabil Shaikh on 18/04/21.
//

import Foundation
import Cucumberish

class CommonStepDefinitions {
    
    func CommonStepDefinitionsImplementation() {
        
        let application = XCUIApplication()
        let login = Login()
        
        Given("User launch the app") { (_, _) -> Void in
            application.launch()
        }
        
        When("User enters username as \"([^\\\"]*)\" and password as \"([^\\\"]*)\"") { (credentials, _) -> Void in
            let username = (credentials?[0])!
            let password = (credentials?[1])!
            login.getUsernameTextField().tap()
            application.typeText(username)
            login.getPasswordTextField().tap()
            application.typeText(password)
        }
        
        MatchAll("User hit Log In button") { (_, _) -> Void in
            login.getLoginButton().tap()
        }
    }
}
