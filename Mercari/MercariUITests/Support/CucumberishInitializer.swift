//
//  CucumberishInitializer.swift
//  MercariUITests
//
//  Created by Nabil Shaikh on 14/04/21.
//

import Foundation
import Cucumberish

class CucumberishInitializer: NSObject {

    @objc class func setupCucumberish(){

        beforeStart { () -> Void in
            CommonStepDefinitions().CommonStepDefinitionsImplementation()
            TC01_ShippingAddress().TC01_ShippingAddressImplementation()
            TC02_SearchItem().TC02_SearchItemImplementation()
        }

        let bundle = Bundle(for: CucumberishInitializer.self)
        Cucumberish.executeFeatures(inDirectory: "Features", from: bundle, includeTags: nil, excludeTags: nil)

    }
}
