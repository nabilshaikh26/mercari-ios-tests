//
//  TC01_ShippingAddress.swift
//  MercariUITests
//
//  Created by Nabil Shaikh on 17/04/21.
//

import Foundation
import Cucumberish

class TC01_ShippingAddress {
    
    func TC01_ShippingAddressImplementation() {
        
        let application = XCUIApplication()
        let home = Home()
        let mypage = MyPage()
        let address = ShippingAddress()
        
        MatchAll("User lands to Home page") { (_, _) -> Void in
            XCTAssertTrue(home.getHomeTabBar().exists)
        }
        
        MatchAll("User visits My Page") { (_, _) -> Void in
            mypage.getMyPageTabBar().tap()
        }
        
        MatchAll("User taps on Personal information") { (_, _) -> Void in
            mypage.getPersonalInformationTab().tap()
        }
        
        MatchAll("User selects Shipping Address") { (_, _) -> Void in
            address.getShippingAddressTab().tap()
        }
        
        // Scenario 1
        
        Given("User is on \"([^\\\"]*)\" page") { (addressList, _) -> Void in
            XCTAssertTrue(address.getAddressListTitle().exists)
        }
        
        MatchAll("User sees Edit and Delete buttons on navigation bar") { (_, _) -> Void in
            XCTAssertTrue(address.getEditButton().exists)
            XCTAssertTrue(address.getDeleteButton().exists)
        }
        
        MatchAll("User sees a placeholder text as \"([^\\\"]*)\"") { (placeholderText, _) -> Void in
            let placeholderString = (placeholderText?[0])!
            XCTAssertTrue(application.staticTexts[placeholderString].exists)
        }
        
        MatchAll("User sees 'Register new address' button at the bottom") { (_, _) -> Void in
            XCTAssertTrue(address.getRegisterAddressButton().exists)
        }
        
        // Scenario 2
        
        MatchAll("User taps on 'Register new address' button") { (_, _) -> Void in
            address.getRegisterAddressButton().tap()
        }
        
        MatchAll("User naviagtes to \"([^\\\"]*)\" page where user sees a form to fill") { (addressRegistration, _) -> Void in
            let addressRegistrationString = (addressRegistration?[0])!
            XCTAssertTrue(application.navigationBars.staticTexts[addressRegistrationString].exists)
        }
        
        MatchAll("User populates all the mandatory fields by providing full name as \"([^\\\"]*)\"") { (fullName, _) -> Void in
            let fullNameString = (fullName?[0])!
            address.getFullNameTextField().tap()
            application.typeText(fullNameString)
            
            address.getAddressTextField().tap()
            application.typeText("206 Sunshine Apartment")
            
            address.getZipcodeTextField().tap()
            application.typeText("400008")
        }
        
        MatchAll("User taps on Save button") { (_, _) -> Void in
            address.getSaveAddressButton().tap()
        }
        
        Then("User should land back to Address list page") { (_, _) -> Void in
            XCTAssertTrue(address.getAddressListTitle().exists)
        }
        
        MatchAll("User sees a message as \"([^\\\"]*)\"") { (message, _) -> Void in
            XCTAssertTrue(address.getSaveMessageTitle(title: (message?[0])!).exists)
        }
        
        MatchAll("User verifies newly added shipping address is present with a name \"([^\\\"]*)\"") { (username, _) -> Void in
            XCTAssertTrue(application.collectionViews.staticTexts[(username?[0])!].exists)
        }
        
        // Scenario 3
        
        When("User selects the shipping address with a name \"([^\\\"]*)\"") { (username, _) -> Void in
            application.collectionViews.staticTexts[(username?[0])!].tap()
        }
        
        MatchAll("User taps on Delete button") { (_, _) -> Void in
            address.getDeleteButton().tap()
        }
        
        Then("User verifies shipping address with a name \"([^\\\"]*)\" is deleted successfully") { (username, _) -> Void in
            XCTAssertFalse(application.collectionViews.staticTexts[(username?[0])!].exists)
        }
    }
}
