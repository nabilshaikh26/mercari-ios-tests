//
//  TC02.swift
//  MercariUITests
//
//  Created by Nabil Shaikh on 18/04/21.
//

import Foundation
import Cucumberish

class TC02_SearchItem {
    
    func TC02_SearchItemImplementation() {
        
        let application = XCUIApplication()
        let home = Home()
        let search = Search()
        let cm = CommonMethods()
        
        When("User search for an item \"([^\\\"]*)\"") { (searchString, _) -> Void in
            home.getSearchTextField().tap()
            application.typeText((searchString?[0])!)
            home.getSearchButton().tap()
        }
        
        Then("User sees all the results realted to \"([^\\\"]*)\"") { (searchString, _) -> Void in
            XCTAssertTrue(application.collectionViews["results"].otherElements.element(boundBy: 0).staticTexts[(searchString?[0])!].exists)
        }
        
        When("User selects the \"([^\\\"]*)\" item from given search results") { (resultIndex, _) -> Void in
            application.collectionViews["results"].otherElements.element(boundBy: cm.parseToInt(string: (resultIndex?[0])!)! - 1).tap()
        }
        
        Then("User navigates to item details page") { (_, _) -> Void in
            XCTAssertTrue(application.navigationBars.staticTexts["Item Details"].exists)
        }
        
        MatchAll("User verifies the item title to include \"([^\\\"]*)\"") { (searchString, _) -> Void in
            let itemTitle = search.getItemTitle().label
            XCTAssertEqual(itemTitle, (searchString?[0])!)
        }
    }
}
