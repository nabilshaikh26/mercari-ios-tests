# Author: Nabil Shaikh

Feature: Search Item

As a user, I want to search for an item of my choice so that I can view the product details and proceed ahead with shopping.

Background: User authentication
    Given User launch the app
    And User enters username as "nabil@gmail.com" and password as "test@123"
    And User hit Log In button
    
Scenario: Search and view an item
    Given User is on "Home" page
    When User search for an item "MacBook"
    Then User sees all the results realted to "MacBook"
    When User selects the "3rd" item from given search results
    Then User navigates to item details page
    And User verifies the item title to include "MacBook"
