# Author: Nabil Shaikh

Feature: Shipping Address

As a user, I want to add new shipping address to my account so that the product of my choice gets delivered to me at my door step.

Background: Navigate to shipping address page after successful authentication
    Given User launch the app
    When User enters username as "nabil@gmail.com" and password as "test@123"
    And User hit Log In button
    Then User lands to Home page
    And User visits My Page
    And User taps on Personal information
    And User selects Shipping Address
    
Scenario: Verify look & feel of shipping address screen
    Given User is on "Address list" page
    And User sees Edit and Delete buttons on navigation bar
    And User sees a placeholder text as "No shipping address has been recorded yet"
    And User sees 'Register new address' button at the bottom
    
Scenario: Add new shipping address
    Given User is on "Address list" page
    When User taps on 'Register new address' button
    Then User naviagtes to "Address registration" page where user sees a form to fill
    When User populates all the mandatory fields by providing full name as "Nabil Shaikh"
    And User taps on Save button
    Then User should land back to Address list page
    And User sees a message as "Address saved!"
    And User verifies newly added shipping address is present with a name "Nabil Shaikh"

Scenario: Delete a shipping address
    Given User is on "Address list" page
    When User selects the shipping address with a name "Nabil Shaikh"
    And User taps on Delete button
    Then User verifies shipping address with a name "Nabil Shaikh" is deleted successfully
    And User sees a placeholder text as "No shipping address has been recorded yet"
